/*global module: false, require: false*/

module.exports = function( grunt ) {
    "use strict";

    grunt.registerTask(
        "default",
        "PUBLIC: Default task that runs the production build",
        [
            "dist"
        ]
    );

    grunt.registerTask(
        "dist",
        "Produces the production files",
        [
            "checkDependencies",
            "test",
            "build"
        ]
    );

    grunt.registerTask(
        "build",
        "Run full build.",
        [
            "clean",
            "js"
        ]
    );

    grunt.registerTask(
        "js",
        "Minify JS",
        [
            "uglify"
        ]
    );

    grunt.registerTask(
        "test",
        "Runs testing tasks",
        [
            "jshint",
            "jscs"
        ]
    );

    // Util
    grunt.util.linefeed = "\n";

    // Config
    grunt.initConfig( {
        pkg: grunt.file.readJSON( "package.json" ),

        // Minify JS
        uglify: {
            options: {
                maxLineLen: 3000
            },
            all: {
                files: [ {
                    expand: true,
                    cwd: "src/js/",
                    src: "*.js",
                    dest: "dist/js/",
                    ext:  ".min.js"
                } ]
            }
        },

        /**
         * Clean demo directories
         *
         * Delete the files/folders in the paths listed below
         */
        clean: {
            dist: [
                "dist/*"
            ]
        },

        // Lint JS
        jshint: {
            options: {
                jshintrc: ".jshintrc"
            },

            all: {
                src: "src/js/*.js"
            }
        },

        // JS coding standards
        jscs: {
            all: {
                options: {
                    config: ".jscsrc"
                },
                src: "src/js/*.js"
            }
        },

        checkDependencies: {
            all: {
                options: {
                    npmInstall: false
                }
            }
        }
    } );

    // Load task libs
    grunt.loadNpmTasks( "grunt-check-dependencies" );     /* Check dependencies */
    grunt.loadNpmTasks( "grunt-contrib-clean" );          /* Delete files/dirs */
    grunt.loadNpmTasks( "grunt-contrib-jshint" );         /* Lint JS */
    grunt.loadNpmTasks( "grunt-contrib-uglify" );         /* Minify JS */
    grunt.loadNpmTasks( "grunt-jscs" );                   /* JS coding standards */
    require( "time-grunt" )( grunt );                     /* Display the elapsed execution time of grunt tasks */
};
