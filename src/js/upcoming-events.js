/**
 * Displays events from an RSS feed, only if they're in the future
 *
 * HTML:
 *  <div id="upcoming" data-url="http://news.gc.ca/web/dsptch-dstrbr-en.do?mthd=xml&crtr.dpt1D=6678&crtr.tp1D=3"></div>
 *  <script src="/eic/site/064.nsf/vwapj/upcoming-events.js/$file/upcoming-events.js"></script>
 *
 * Refs:
 *  EPI-7448
 *  EPI-10938
 */

( function() {
    window.addEventListener( "load", function() {
        window.iccb = function( data ) {
            var feeds = data.responseData.feed,
                lang = wb.lang,
                html = "<ul>",
                noEventsMsg = ( lang === "en" ) ? "There are no current events." : "Aucun &eacute;v&eacute;nement en cours.",
                frenchSpace = ( lang === "en" ) ? "" : "&nbsp;",
                $elm = $( "#upcoming" ),
                skipped = 0,
                i,
                entry,
                date,
                p,
                expDate,
                month,
                engDate,
                dictionary;

            dictionary = function( pword ) {
                switch ( pword ) {
                    case "janvier":
                        return "January";
                    case "février":
                        return "February";
                    case "mars":
                        return "March";
                    case "avril":
                        return "April";
                    case "mai":
                        return "May";
                    case "juin":
                        return "June";
                    case "juillet":
                        return "July";
                    case "août":
                        return "August";
                    case "septembre":
                        return "September";
                    case "octobre":
                        return "October";
                    case "novembre":
                        return "November";
                    case "décembre":
                        return "December";
                }
            };

            // Check for errors
            if ( !feeds ) {
                return false;
            }

            // If no items in feed
            if ( feeds.entries.length === 0 ) {
                $elm.parent().append( noEventsMsg );

                return;
            }

            for ( i = 0; i < feeds.entries.length; i += 1 ) {
                entry = feeds.entries[i];

                date = "";
                p = $( "<p>" );
                p.html( entry.content );

                expDate = "";

                // If there is a <span> in the description, it contains the "Date of Event"
                if ( $( "span", p ).length ) {

                    date = $( "span", p ).get( 0 );
                    date = $( date ).text();

                    if ( lang === "fr" ) { /* If we're on a French page, we're presumably pulling a French feed. */
                        month = date.split( " " )[ 1 ]; /* Find the French month */
                        engDate = date.replace( month, dictionary( month ) ); /* Convert it in English */
                        expDate = Date.parse( engDate ) + 86400000; /* Date of event + one day (in milliseconds) */
                    } else {
                        expDate = Date.parse( date ) + 86400000; /* Date of event + one day (in milliseconds) */
                    }

                    if ( expDate < new Date() || isNaN( expDate ) ) { /* If the date of event is passed */
                        skipped++;
                        continue; /* Don't display the event */
                    }

                    // We want a max of 3 events
                    if ( i + 1 - skipped >= 4 && feeds.entries.length >= 4 ) {
                        break;
                    }

                } else {
                    date = wb.toDateISO( Date.parse( entry.publishedDate ) );
                }

                // create the source code for the feed
                html += "<li><span class='rss-date'>" + date +
                    "</span><span class='rss-dash'>" + frenchSpace +
                    "&#8212;</span>" + frenchSpace + "<a href='" + entry.link +
                    "' class='sharewidget-notice'>" + entry.title +
                    "</a><span class='sharewidget-details'></span></li>";
            }

            if ( skipped >= feeds.entries.length ) {
                $elm.parent().append( noEventsMsg );
            }

            $elm.html( html + "</ul>" );
        };

        var scr = document.createElement( "script" ),
            $elm = $( "#upcoming" ),
            url = $elm.data( "url" ),
            gUrl = "https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&callback=iccb&q=" +
                encodeURIComponent( decodeURIComponent( url ) ) +
                "&num=5";

        scr.src = gUrl;
        $( "head" ).append( scr );
    } );
}() );
