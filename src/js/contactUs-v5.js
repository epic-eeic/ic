/* TODOs:
    // Merge duplicate code
*/

/*global $: false, ic: false, google: false*/

window.addEventListener( "load", function() {
    "use strict";


    var urlParam,
        sTypeVal,
        cseType,
        sFromVal,
        sSituationVal,
        sEnquiryVal,
        pageid,
        divToAPpendTo,
        buildSubDP,
        buildSubjectDP,
        buildSubSubjectDP,
        buildBinders,
        buildSubjectBinders,
        buildSubSubjectBinders,
        buildLastBinders,
        buildMainDP,
        deconstruct,
        ref,
        construct,
        showResults,
        relatedQuestions,
        customSearchControl,
        filter = "",
        generateSID,
        sid,
        queryAddition = "",
        lang = ( $( "html" ).attr( "lang" ) === "en" ) ? "eng" : "fra",
        dic = {
            "backToMC": ( lang === "eng" ) ? "Back to Measurement Canada" : "Retour &agrave; Mesures Canada",
            "fin": ( lang === "eng" ) ? "Financing" : "Financement",
            "csbfp": ( lang === "eng" ) ? "Canada Small Business Financing Program" : "Programme de financement des petites entreprises du Canada",
            "media": ( lang === "eng" ) ? "Media" : "Demandes",
            "ito": ( lang === "eng" ) ? "Industrial Technologies Office" : "Office des technologies industrielles",
            "gen": ( lang === "eng" ) ? "I have a general question" : "Jai une question dordre g\xE9n\xE9ral",
            "weights": ( lang === "eng" ) ? "Weights and Measures" : "Poids et mesures"
        };

    // Generates a random number which we use as unique session id
    generateSID = function() {
        var sid = "PSNS-",
            i;

        for ( i = 0; i < 6; i += 1 ) {
            sid += Math.floor( ( Math.random() * 10 ) + 1 );
        }

        return sid;
    };

    sid = generateSID();

    // Returns the value of the 'name' URL parameter
    urlParam = function( name ) {
        var results = new RegExp( "[\\?&#]" + name + "=([^&#]*)" ).exec( window.location.href );

        if ( results !== null ) {
            return results[ 1 ] || false;
        }
    };

    ref = document.referrer;
    pageid = urlParam( "pageid" );

    if ( pageid === undefined ) {
        pageid = "n/a";
    }

    sTypeVal = urlParam( "type" );
    sFromVal = urlParam( "from" );
    sSituationVal = urlParam( "situation" );
    sEnquiryVal = urlParam( "enquiry" );
    divToAPpendTo = "#" + sTypeVal;

    relatedQuestions = function( type ) {
        var query = "",
            topic = $( "#select_" + type + "_main option:selected" ).text(),
            $mainDropdown = $( "#select_" + type + "_main" ),
            $subject = $( "#subject_" + type ),
            $container = $( "#" + type ).closest( "details" ).find( ".ic-rltd-q" );

        cseType = type;

        // No related questions for Media Inquiries
        if ( $mainDropdown.val().split( " " )[ 0 ] === dic.media ) {
            $container.fadeOut();
            return;
        }

        // No related questions for ITO general inquires
        if ( $subject.val() === dic.gen ) {
            $container.fadeOut();
            return;
        }

        if ( $( "#sub_" + type ).val() === dic.ito && $mainDropdown.val() === dic.fin && ( $subject.val() === undefined || $subject.get( 0 ).selectedIndex === 0 ) ) {
            $container.fadeOut();
            return;
        }

        if ( type === "email" ) {
            query = $( "#Body" ).val() || "";
            query += " ";
            query += $( "#Subject" ).val() || "";
        }

        if ( $container.find( "#cse-" + type ).length === 0 ) {
            $container.append( $( "<div id='cse-wrap-" + type + "'><div id='cse-" + type + "'></div></div>" ) );
        }

        if ( customSearchControl ) {
            /* Draw the Custom Search Control in the div named "cse" */
            customSearchControl.draw( "cse-" + type );

            $( "#cse-wrap-" + type ).parent().fadeOut();
            customSearchControl.execute( query + " " + topic + " more:pagemap:question" + " " + filter + " " + queryAddition );
        }
    };

    // TODO: Document
    buildSubDP = function( fctFromVal ) {
        buildBinders( $( "#select_" + sTypeVal + "_main" ).val(), "all" );
        $.ajax( {
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-dp-" + lang + ".xml/$FILE/cu-cn-dp-" + lang + ".xml",
            async: false,
            success: function( xml ) {
                $( xml ).find( "dropdownsubmain[type='" + sTypeVal + "'][business='" + fctFromVal + "']" ).each( function() {
                    var $this = $( this ),
                        label = $this.find( "label" ).text(),
                        dict = {
                            "inb": ( lang === "eng" ) ? "Industries and Business" : "Industries et entreprises",
                            "rns": ( lang === "eng" ) ? "SME Research and Statistics" : "Recherche et statistique sur la petite entreprise"
                        },
                        fromSME = ( fctFromVal === dict.inb ) && ( document.referrer.match( /061\.nsf/ ) );

                    if ( divToAPpendTo === "#email" ) {
                        divToAPpendTo = ".email-in";
                    } else if ( divToAPpendTo === "#phone" ) {
                        divToAPpendTo = ".phone-in";
                    } else if ( divToAPpendTo === "#visit" ) {
                        divToAPpendTo = ".visit-in";
                    }

                    $( "<label for='sub_" + sTypeVal + "'  class='select_" + sTypeVal + "_sub_main'></label>" ).html( label ).appendTo( divToAPpendTo );
                    $( "<select id='sub_" + sTypeVal + "' class='select_sub_main form-control mrgn-bttm-lg'></select>" ).html( "" )
                        .appendTo( divToAPpendTo )
                        .on( "focusin", function() {
                            $( this ).addClass( "focus" );
                        } )
                        .on( "focusout", function() {
                            $( this ).removeClass( "focus" );
                        } );

                    $this.find( "option" ).each( function() {
                        var $this = $( this ),
                            value = $this.val(),
                            classes = $( this ).attr( "class" ) || "",
                            text  = $this.text();

                        if ( text !== dict.rns || fromSME ) {
                            $( "<option class='" + classes + "' value='" + value + "'></option>" ).html( text ).appendTo( "#sub_" + sTypeVal );
                        }
                    } );
                } );
            }
        } );
    };

    // TODO: Document
    buildSubjectDP = function( fctBusiness ) {
        $( ".select_" + sTypeVal + "_subject" ).remove();
        $( "#subject_" + sTypeVal ).remove();
        $( "#sub_subject_" + sTypeVal ).remove();
        $( ".select_" + sTypeVal + "_sub_subject" ).remove();
        $( "#binders_all_subject_" + sTypeVal ).remove();
        $( "#binders_" + sTypeVal ).remove();

        if ( $( "#sub_" + sTypeVal ).val() !== "" ) {
            buildSubjectBinders( $( "#select_" + sTypeVal + "_main" ).val(), $( "#sub_" + sTypeVal ).val(), "all" );
        }

        var counter = 0;
        $.ajax( {
            async: false,
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-dp-" + lang + ".xml/$FILE/cu-cn-dp-" + lang + ".xml",
            success: function( xml ) {
                $( xml ).find( "dropdownsubmainsubject[type='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + $( "#sub_" + sTypeVal ).val() + "']" ).each( function() {
                    counter += 1;
                    var label = $( this ).find( "label" ).text();

                    if ( divToAPpendTo === "#email" ) {
                        divToAPpendTo = ".email-in";
                    } else if ( divToAPpendTo === "#phone" ) {
                        divToAPpendTo = ".phone-in";
                    } else if ( divToAPpendTo === "#visit" ) {
                        divToAPpendTo = ".visit-in";
                    }

                    $( "<label for='subject_" + sTypeVal + "' class='select_" + sTypeVal + "_subject'></label>" ).html( label ).appendTo( divToAPpendTo );
                    $( "<select name='subject_" + sTypeVal + "' id='subject_" + sTypeVal + "' class='select_subject form-control'></select>" ).html( "" ).appendTo( divToAPpendTo );

                    $( this ).find( "option" ).each( function() {
                        var value = $( this ).val(),
                            classes = $( this ).attr( "class" ) || "",
                            text  = $( this ).text();
                        $( "<option class='" + classes + "' value='" + value + "'></option>" ).html( text ).appendTo( "#subject_" + sTypeVal );
                    } );
                } );

                if ( counter === 0 && $( "#sub_" + sTypeVal ).val() !== "" ) {
                    buildSubjectBinders( $( "#select_" + sTypeVal + "_main" ).val(), $( "#sub_" + sTypeVal ).val(), $( "#sub_" + sTypeVal ).prop( "selectedIndex" ) + 1 );
                }
            }
        } );
    };

    // TODO: Document
    buildSubSubjectDP = function( fctBusiness ) {
        $( ".select_" + sTypeVal + "_sub_subject" ).remove();
        $( "#sub_subject_" + sTypeVal ).remove();
        $( "#binders_" + sTypeVal ).remove();

        var counter = 0;
        $.ajax( {
            async: false,
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-dp-" + lang + ".xml/$FILE/cu-cn-dp-" + lang + ".xml",
            success: function( xml ) {
                $( xml ).find( "dropdownsubmainsubsubject[type='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + $( "#sub_" + sTypeVal ).val() + "'][subsubject='" + $( "#subject_" + sTypeVal ).val() + "']" ).each( function() {
                    counter += 1;
                    var label = $( this ).find( "label" ).text();

                    if ( divToAPpendTo === "#email" ) {
                        divToAPpendTo = ".email-in";
                    } else if ( divToAPpendTo === "#phone" ) {
                        divToAPpendTo = ".phone-in";
                    } else if ( divToAPpendTo === "#visit" ) {
                        divToAPpendTo = ".visit-in";
                    }

                    $( "<label class='select_" + sTypeVal + "_sub_subject' for='sub_subject_" + sTypeVal + "'></label>" ).html( label ).appendTo( divToAPpendTo );
                    $( "<select name='" + sTypeVal + "' id='sub_subject_" + sTypeVal + "' class='select_sub_subject form-control'></select>" ).html( "" ).appendTo( divToAPpendTo );

                    $( this ).find( "option" ).each( function() {
                        var value = $( this ).val(),
                            classes = $( this ).attr( "class" ) || "",
                            text = $( this ).text();
                        $( "<option class='" + classes + "' value='" + value + "'></option>" ).html( text ).appendTo( "#sub_subject_" + sTypeVal );
                    } );
                } );

                if ( counter === 0 ) {
                    buildSubSubjectBinders( $( "#select_" + sTypeVal + "_main" ).val(), $( "#sub_" + sTypeVal ).val(), $( "#subject_" + sTypeVal ).prop( "selectedIndex" )  + 1 );
                }
            }
        } );
    };

    buildBinders = function( fctBusiness, fctSelection ) {
        var email = "",
            sHTMLtoDisplay;

        $.ajax( {
            async: false,
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-" + sTypeVal + "-" + lang + ".xml/$FILE/cu-cn-" + sTypeVal + "-" + lang + ".xml",
            success: function( xml ) {
                var done = false;

                if ( fctSelection === "all" ) {
                    sHTMLtoDisplay = "<div id='binders_all_" + sTypeVal + "' class='select_" + sTypeVal + "_sub_main'>";
                } else {
                    sHTMLtoDisplay = "<div id='binders_" + sTypeVal + "'>";
                }

                $( xml ).find( "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][subject=''][selection='" + fctSelection + "']" ).each( function() {
                    if ( divToAPpendTo === "#email" ) {
                        divToAPpendTo = ".email-in";
                    } else if ( divToAPpendTo === "#phone" ) {
                        divToAPpendTo = ".phone-in";
                    } else if ( divToAPpendTo === "#visit" ) {
                        divToAPpendTo = ".visit-in";
                    }

                    $( this ).find( "contact" ).each( function() {
                        var sbtitle = $( this ).find( "sbtitle" ).text(),
                            sbinfo;
                        if ( sTypeVal === "email" ) {
                            sbinfo = $( this ).find( "sbinfo" ).text();
                        } else {
                            sbinfo = $( this ).find( "sbinfo" ).text().replace( /\n/g, "<br />" );
                        }

                        if ( sbtitle !== "" ) {
                            sHTMLtoDisplay  += "<div style='margin-bottom: 15px;'><h3 class='ic-cntct-ttl'>" + sbtitle + "</h3>";
                        }

                        sHTMLtoDisplay += sbinfo;

                        if ( sbtitle !== "" ) {
                            sHTMLtoDisplay += "</div>";
                        }
                        email = $( this ).find( "email" ).text();
                    } );

                    sHTMLtoDisplay  += "</div>";
                    $( divToAPpendTo ).append( sHTMLtoDisplay );

                    done = true;
                } );

                if ( sTypeVal === "email" && email !== "" && fctSelection === "all" ) {
                    $.ajax( {
                        url: "/eic/site/icgc.nsf/vwapj/" + sTypeVal + "-" + lang + ".inc/$FILE/" + sTypeVal + "-" + lang + ".inc",
                        dataType: "html",
                        async: false,
                        success: function( data ) {
                            $( "#binders_all_" + sTypeVal ).append( data );
                            $( "#hid_pageid" ).attr( "value", pageid );
                            relatedQuestions( "email" );
                            $( "#hid_pageid" ).attr( "value", pageid );
                            $( "#hid_referrer" ).attr( "value", ref );
                            if ( $( "#sub_email" ).val() === dic.csbfp ) {
                                $( ".csbfp" ).css( "display", "block" ); /* label, select */
                                $( "#csbfp" ).addClass( "required" );    /* select */
                            } else {
                                $( ".csbfp" ).attr( "style", "display: none !important;" );  /* label, select */
                                $( "#csbfp" ).removeClass( "required" ); /* select */
                            }

                            if ( $( "#select_email_main" ).val() === dic.weights && $( "#sub_email option:selected" ).hasClass( "att" ) ) {
                                $( "#attachment" ).show();
                            }

                            $( "[name='$sid']" ).val( sid );

                            wb.add( "main .wb-frmvld" );
                        }
                    } );

                    $( "#to" ).attr( "value", email );

                    $( "#hid_select_email_main" ).attr( "value", $( "#select_email_main option:selected" ).text() );

                    if ( email === "ic.info-info.ic@canada.ca" ) {
                        $( "#_SRT" ).attr( "action", "/imcc/internet.nsf/IC-" + lang.charAt( 0 ).toUpperCase() + lang.slice( 1 ) + "?CreateDocument" );
                    }

                    $( "#contact-info-box" ).hide();

                    done = true;
                }
            }
        } );
    };

    buildSubjectBinders = function( fctBusiness, fctSubject, fctSelection ) {
        var counter = 0,
            email = "",
            sHTMLtoDisplay = "";

        $.ajax( {
            async: false,
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-" + sTypeVal + "-" + lang + ".xml/$FILE/cu-cn-" + sTypeVal + "-" + lang + ".xml",
            success: function( xml ) {
                var done = false;
                if ( fctSelection === "all" ) {
                    sHTMLtoDisplay = "<div id='binders_all_subject_" + sTypeVal + "' class='select_" + sTypeVal + "_sub_main'>";
                    $( xml ).find( "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + fctSubject + "'][selection='" + fctSelection + "']" ).each( function() {
                        $( this ).find( "contact" ).each( function() {
                            var sbtitle = $( this ).find( "sbtitle" ).text(),
                                sbinfo;

                            if ( sTypeVal === "email" ) {
                                sbinfo = $( this ).find( "sbinfo" ).text();
                            } else {
                                sbinfo = $( this ).find( "sbinfo" ).text().replace( /\n/g, "<br />" );
                            }

                            if ( sbtitle !== "" ) {
                                sHTMLtoDisplay  += "<div style='margin-bottom: 15px;'><h3 class='ic-cntct-ttl'>" + sbtitle + "</h3>";
                            }

                            sHTMLtoDisplay  += sbinfo;

                            if ( sbtitle !== "" ) {
                                sHTMLtoDisplay += "</div>";
                            }
                        } );

                        done = true;
                    } );
                } else {
                    sHTMLtoDisplay = "<div id='binders_" + sTypeVal + "'>";
                    $( xml ).find( "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + fctSubject + "']" ).each( function() {
                        counter += 1;

                        $( this ).find( "contact" ).each( function() {
                            var sbtitle = $( this ).find( "sbtitle" ).text(),
                                sbinfo;

                            if ( sTypeVal === "email" ) {
                                sbinfo = $( this ).find( "sbinfo" ).text();
                            } else {
                                sbinfo = $( this ).find( "sbinfo" ).text().replace( /\n/g, "<br />" );
                            }

                            if ( sbtitle !== "" ) {
                                sHTMLtoDisplay  += "<h3 class='ic-cntct-ttl'>" + sbtitle + "</h3>";
                            }

                            sHTMLtoDisplay  += sbinfo;
                            email = $( this ).find( "email" ).text();
                        } );

                        done = true;
                    } );
                }

                sHTMLtoDisplay  += "</div>";
                if ( divToAPpendTo === "#email" ) {
                    divToAPpendTo = ".email-in";
                } else if ( divToAPpendTo === "#phone" ) {
                    divToAPpendTo = ".phone-in";
                } else if ( divToAPpendTo === "#visit" ) {
                    divToAPpendTo = ".visit-in";
                }
                $( divToAPpendTo ).append( sHTMLtoDisplay );

                if ( sTypeVal === "email" && email !== "" && fctSelection !== "all" ) {
                    $.ajax( {
                        url: "/eic/site/icgc.nsf/vwapj/" + sTypeVal + "-" + lang + ".inc/$FILE/" + sTypeVal + "-" + lang + ".inc",
                        success: function( data ) {
                            $( "#binders_" + sTypeVal ).append( data );
                            relatedQuestions( "email" );
                            $( "#hid_pageid" ).attr( "value", pageid );
                            $( "#hid_referrer" ).attr( "value", ref );
                            if ( $( "#sub_email" ).val() === dic.csbfp ) {
                                $( ".csbfp" ).css( "display", "block" ); /* label, select */
                                $( "#csbfp" ).addClass( "required" );    /* select */
                            } else {
                                $( ".csbfp" ).attr( "style", "display: none !important;" );  /* label, select */
                                $( "#csbfp" ).removeClass( "required" ); /* select */
                            }

                            if ( $( "#select_email_main" ).val() === dic.weights && $( "#sub_email option:selected" ).hasClass( "att" ) ) {
                                $( "#attachment" ).show();
                            }

                            $( "[name='$sid']" ).val( sid );

                            wb.add( "main .wb-frmvld" );
                        },
                        dataType: "html",
                        async: false
                    } );

                    $( "#to" ).attr( "value", email );
                    $( "#hid_select_email_main" ).attr( "value", $( "#select_email_main option:selected" ).text() );
                    $( "#hid_sub_email" ).attr( "value", $( "#sub_email option:selected" ).text() );

                    if ( email === "ic.info-info.ic@canada.ca" ) {
                        $( "#_SRT" ).attr( "action", "/imcc/internet.nsf/IC-" + lang.charAt( 0 ).toUpperCase() + lang.slice( 1 ) + "?CreateDocument" );
                    }

                    $( "#contact-info-box" ).hide();

                    done = true;
                }
            }
        } );
    };

    buildSubSubjectBinders = function( fctBusiness, fctSubject, fctSelection ) {
        $( "#binders_" + sTypeVal ).remove();

        var email = "";
        $.ajax( {
            async: false,
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-" + sTypeVal + "-" + lang + ".xml/$FILE/cu-cn-" + sTypeVal + "-" + lang + ".xml",
            success: function( xml ) {
                var sHTMLtoDisplay,
                    toFind,
                    done = false;

                if ( fctSelection === "all" ) {
                    sHTMLtoDisplay = "<div id='binders_all_sub_subject_" + sTypeVal + "' class='select_" + sTypeVal + "_sub_main'>";
                    $( xml ).find( "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + fctSubject + "'][selection='" + fctSelection + "']" ).each( function() {
                        $( this ).find( "contact" ).each( function() {
                            var sbtitle = $( this ).find( "sbtitle" ).text(),
                                sbinfo;

                            if ( sTypeVal === "email" ) {
                                sbinfo = $( this ).find( "sbinfo" ).text();
                            } else {
                                sbinfo = $( this ).find( "sbinfo" ).text().replace( /\n/g, "<br />" );
                            }

                            if ( sbtitle !== "" ) {
                                sHTMLtoDisplay  += "<h3 class='ic-cntct-ttl'>" + sbtitle + "</h3>";
                            }

                            sHTMLtoDisplay  += sbinfo;
                            email = $( this ).find( "email" ).text();
                        } );

                        done = true;
                    } );
                } else {
                    sHTMLtoDisplay = "<div id='binders_" + sTypeVal + "'>";
                    toFind = "";

                    if ( sTypeVal !== "email" ) {
                        toFind = "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + fctSubject + "'][selection='" + fctSelection + "']";
                    } else {
                        toFind = "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + $( "#subject_" + sTypeVal ).val() + "'][sitename='" + fctSubject + "']";
                    }

                    $( xml ).find( toFind ).each( function() {
                        $( this ).find( "contact" ).each( function() {
                            var sbtitle = $( this ).find( "sbtitle" ).text(),
                                sbinfo;

                            if ( sTypeVal === "email" ) {
                                sbinfo = $( this ).find( "sbinfo" ).text();
                            } else {
                                sbinfo = $( this ).find( "sbinfo" ).text().replace( /\n/g, "<br />" );
                            }

                            if ( sbtitle !== "" ) {
                                sHTMLtoDisplay  += "<h3 class='ic-cntct-ttl'>" + sbtitle + "</h3>";
                            }

                            sHTMLtoDisplay  += sbinfo;
                            email = $( this ).find( "email" ).text();
                        } );

                        done = true;
                    } );
                }

                sHTMLtoDisplay  += "</div>";
                if ( divToAPpendTo === "#email" ) {
                    divToAPpendTo = ".email-in";
                } else if ( divToAPpendTo === "#phone" ) {
                    divToAPpendTo = ".phone-in";
                } else if ( divToAPpendTo === "#visit" ) {
                    divToAPpendTo = ".visit-in";
                }
                $( divToAPpendTo ).append( sHTMLtoDisplay );

                if ( sTypeVal === "email" && email !== "" && fctSelection !== "all" ) {
                    $.ajax( {
                        url: "/eic/site/icgc.nsf/vwapj/" + sTypeVal + "-" + lang + ".inc/$FILE/" + sTypeVal + "-" + lang + ".inc",
                        success: function( data ) {
                            $( "#binders_" + sTypeVal ).append( data );
                            relatedQuestions( "email" );
                            $( "#hid_pageid" ).attr( "value", pageid );
                            $( "#hid_referrer" ).attr( "value", ref );
                            if ( $( "#sub_email" ).val() === dic.csbfp ) {
                                $( ".csbfp" ).css( "display", "block" ); /* label, select */
                                $( "#csbfp" ).addClass( "required" );    /* select */
                            } else {
                                $( ".csbfp" ).attr( "style", "display: none !important;" );  /* label, select */
                                $( "#csbfp" ).removeClass( "required" ); /* select */
                            }
                            if ( $( "#select_email_main" ).val() === dic.weights && $( "#sub_email option:selected" ).hasClass( "att" ) ) {
                                $( "#attachment" ).show();
                            }

                            $( "[name='$sid']" ).val( sid );

                            wb.add( "main .wb-frmvld" );
                        },
                        dataType: "html",
                        async: false
                    } );

                    $( "#to" ).attr( "value", email );
                    $( "#hid_select_email_main" ).attr( "value", $( "#select_email_main option:selected" ).text() );
                    $( "#hid_sub_email" ).attr( "value", $( "#sub_email option:selected" ).text() );

                    if ( email === "ic.info-info.ic@canada.ca" ) {
                        $( "#_SRT" ).attr( "action", "/imcc/internet.nsf/IC-" + lang.charAt( 0 ).toUpperCase() + lang.slice( 1 ) + "?CreateDocument" );
                    }

                    $( "#contact-info-box" ).hide();

                    done = true;
                }
            }
        } );
    };

    buildLastBinders = function( fctBusiness, fctSitename, fctSelection, fctSubSubject, fctSubject ) {
        $( "#binders_" + sTypeVal ).remove();

        var email = "";

        $.ajax( {
            async: false,
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-" + sTypeVal + "-" + lang + ".xml/$FILE/cu-cn-" + sTypeVal + "-" + lang + ".xml",
            success: function( xml ) {
                var sHTMLtoDisplay = "<div id='binders_" + sTypeVal + "'>",
                    toFind = "",
                    done = false;
                if ( fctSelection !== "all" ) {
                    if ( sTypeVal !== "email" ) {
                        toFind = "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][sitename='" + fctSitename + "'][subject='" + fctSubject + "'][selection='" + fctSelection + "']";
                    } else {
                        toFind = "binder[category='" + sTypeVal + "'][business='" + fctBusiness + "'][subject='" + fctSubject + "'][sitename='" + fctSitename + "']";
                    }

                    $( xml ).find( toFind ).each( function() {
                        $( this ).find( "contact" ).each( function() {
                            var sbtitle = $( this ).find( "sbtitle" ).text(),
                                sbinfo;

                            if ( sTypeVal === "email" ) {
                                sbinfo = $( this ).find( "sbinfo" ).text();
                            } else {
                                sbinfo = $( this ).find( "sbinfo" ).text().replace( /\n/g, "<br />" );
                            }

                            if ( sbtitle !== "" ) {
                                sHTMLtoDisplay  += "<h3 class='ic-cntct-ttl'>" + sbtitle + "</h3>";
                            }

                            sHTMLtoDisplay  += sbinfo;
                            email = $( this ).find( "email" ).text();
                        } );

                        done = true;
                    } );
                }

                sHTMLtoDisplay  += "</div>";
                if ( divToAPpendTo === "#email" ) {
                    divToAPpendTo = ".email-in";
                } else if ( divToAPpendTo === "#phone" ) {
                    divToAPpendTo = ".phone-in";
                } else if ( divToAPpendTo === "#visit" ) {
                    divToAPpendTo = ".visit-in";
                }
                $( divToAPpendTo ).append( sHTMLtoDisplay );

                if ( sTypeVal === "email" && email !== "" && fctSelection !== "all" ) {
                    $.ajax( {
                        async: false,
                        url: "/eic/site/icgc.nsf/vwapj/" + sTypeVal + "-" + lang + ".inc/$FILE/" + sTypeVal + "-" + lang + ".inc",
                        success: function( data ) {
                            $( "#binders_" + sTypeVal ).append( data );
                            $( "#hid_pageid" ).attr( "value", pageid );
                            $( "#hid_referrer" ).attr( "value", ref );

                            if ( $( "#sub_email" ).val() === dic.csbfp ) {
                                $( "#Subject, label[for='Subject']" ).attr( "style", "display: none !important;" );
                                $( "#Subject" ).val( $( "#sub_subject_email" ).val() );
                            } else {
                                $( "#Subject, label[for='Subject']" ).show();
                                $( "#Subject" ).val( "" );
                            }

                            if ( $( "#sub_email" ).val() === dic.csbfp ) {
                                $( ".csbfp" ).css( "display", "block" ); /* label, select */
                                $( "#csbfp" ).addClass( "required" );    /* select */
                            } else {
                                $( ".csbfp" ).attr( "style", "display: none !important;" );  /* label, select */
                                $( "#csbfp" ).removeClass( "required" ); /* select */
                            }

                            if ( $( "#select_email_main" ).val() === dic.fin && $( "#sub_email" ).val() === dic.csbfp ) {
                                $( "#hid_subject_email" ).val( $( "#subject_email" ).val() );
                                $( "#hid_sub_subject_email" ).val( $( "#sub_subject_email" ).val() );
                            }
                            if ( $( "#select_email_main" ).val() === dic.weights && $( "#sub_email option:selected" ).hasClass( "att" ) ) {
                                $( "#attachment" ).show();
                            }

                            $( "[name='$sid']" ).val( sid );

                            relatedQuestions( "email" );

                            wb.add( "main .wb-frmvld" );
                        }
                    } );

                    $( "#to" ).attr( "value", email );
                    $( "#hid_select_email_main" ).attr( "value", $( "#select_email_main option:selected" ).text() );
                    $( "#hid_sub_email" ).attr( "value", $( "#sub_email option:selected" ).text() );

                    if ( email === "ic.info-info.ic@canada.ca" ) {
                        $( "#_SRT" ).attr( "action", "/imcc/internet.nsf/IC-" + lang.charAt( 0 ).toUpperCase() + lang.slice( 1 ) + "?CreateDocument" );
                    }

                    $( "#contact-info-box" ).hide();

                    done = true;
                }
            }
        } );
    };

    buildMainDP = function() {
        $.ajax( {
            async: false,
            url: "/eic/site/icgc.nsf/vwapj/cu-cn-dp-" + lang + ".xml/$FILE/cu-cn-dp-" + lang + ".xml",
            success: function( xml ) {
                $( xml ).find( "dropdownmain" ).each( function() {
                    var label = $( this ).find( "label" ).text(),
                        sHTMLtoDisplay = "";
                    sHTMLtoDisplay += "<div class='form-group'><label for='select_" + sTypeVal + "_main'>" + label + "</label>";
                    sHTMLtoDisplay += "<select id='select_" + sTypeVal + "_main' class='select_main form-control'></div>";

                    $( this ).find( "option" ).each( function() {
                        var value = $( this ).val(),
                            classes = $( this ).attr( "class" ) || "",
                            text = $( this ).text();
                        sHTMLtoDisplay += "<option class='" + classes + "' value='" + value + "'>" + text + "</option>";
                    } );

                    sHTMLtoDisplay += "</select>";
                    if ( divToAPpendTo === "#email" ) {
                        divToAPpendTo = ".email-in";
                    } else if ( divToAPpendTo === "#phone" ) {
                        divToAPpendTo = ".phone-in";
                    } else if ( divToAPpendTo === "#visit" ) {
                        divToAPpendTo = ".visit-in";
                    }
                    $( divToAPpendTo ).append( sHTMLtoDisplay );
                } );
            }
        } );
    };

    deconstruct = function( intLevel ) {
        if ( $( "#binders_" + sTypeVal ).length > 0 ) {
            $( "#binders_" + sTypeVal ).remove();
            $( "#subject_" + sTypeVal ).remove();
            $( ".select_" + sTypeVal + "_subject" ).remove();
        }

        if ( $( "#sub_" + sTypeVal ).length > 0 && intLevel > 1 ) {
            $( "#sub_" + sTypeVal ).remove();
            $( ".select_" + sTypeVal + "_sub_main" ).remove();
            $( ".select_" + sTypeVal + "_subject" ).remove();
            $( ".select_" + sTypeVal + "_sub_subject" ).remove();
            $( "#sub_subject_" + sTypeVal ).empty().remove();
        }

        if ( $( "#subject_" + sTypeVal ).length > 0 && intLevel === 0 ) {
            $( "#subject_" + sTypeVal ).remove();
        }

        if ( intLevel > 1 ) {
            $( "#binders_all_" + sTypeVal ).remove();
            $( "#subject_" + sTypeVal ).remove();
        }
    };

    construct = function( fctType ) {
        sTypeVal = fctType;
        divToAPpendTo = "#" + fctType;

        // Create new HTML panel if it's the first time it expand
        if ( $( "#select_" + sTypeVal + "_main" ).length === 0 ) {
            buildMainDP( fctType );
        }
    };

    showResults = function() {
        var $qContainer = $( "#" + cseType ).closest( "details" ).find( ".ic-rltd-q" ),
            $ddContainer = $( "." + cseType + "-in" ),
            $containers = $qContainer.add( $ddContainer );

        if ( $qContainer.find( ".gsc-result li a" ).length ) {
            $containers.css( "min-height", "196px" );

            // Same height as left-col without cutting links
            while ( $qContainer.height() > $ddContainer.height() ) {
                $qContainer.find( ".gsc-result li.ic-result-li" ).last().remove();

                // Infinite loop safeguard
                if ( $qContainer.find( ".gsc-result li a" ).length === 0 ) {
                    break;
                }
            }

            $qContainer.fadeIn();
        } else {
            $containers.css( "min-height", "" );
        }
    };

    // Init
    ( function() {
        var cx = ( lang === "eng" ) ? "016714752915828769485:fm-x8iyi1cu" : "016714752915828769485:d_cv_h6v74c",
            $trgt;

        if ( ic.search.google ) {
            customSearchControl = new google.search.CustomSearchControl( cx );
            customSearchControl.setSearchCompleteCallback( this, showResults, null );
            google.search.Csedr.addOverride( "ic-" );
        }

        // ICGCWEB-423
        if ( document.referrer.match( /mc-mc\.nsf/ ) ) {
            $( ".ic-subbanner" ).not( ".eic-subbanner" ).after( "<p><a href='" + document.referrer + "'>" + dic.backToMC +  "</a></p>" );
        }

        buildMainDP( sTypeVal );

        $( "body" ).on( "change", "#select_email_main", function() {
            filter = "";
            queryAddition = "";

            for ( var property in ic.search.topics ) {
                if ( ic.search.topics.hasOwnProperty( property ) ) {
                    if ( ic.search.topics[ property ].label.toLowerCase() === $( this ).val().toLowerCase() ) {
                        filter = ic.search.topics[ property ].data;
                    }
                }
            }

            if ( $( this ).get( 0 ).selectedIndex !== 0 ) {
                relatedQuestions( "email" );
            } else {
                $( "#email" ).closest( "details" ).find( ".ic-rltd-q" ).fadeOut();
            }
        } );

        $( "body" ).on( "change", "#sub_email", function() {
            if ( $( "#sub_email" ).val() === dic.ito ) {
                $( "#email" ).closest( "details" ).find( ".ic-rltd-q" ).fadeOut();
                return;
            }
        } );

        $( "body" ).on( "change", "#subject_email", function() {
            if ( $( this ).get( 0 ).selectedIndex !== 0 ) {
                queryAddition = $( this ).val();
            } else {
                queryAddition = "";
            }

            relatedQuestions( "email" );
        } );

        $( "body" ).on( "keyup", "#Body, #Subject", function( e ) {
            if ( e.keyCode === 32 ) {
                relatedQuestions( "email" );
            }
        } );

        $( "body" ).on( "change", "#select_phone_main", function() {
            filter = "";
            queryAddition = "";

            for ( var property in ic.search.topics ) {
                if ( ic.search.topics.hasOwnProperty( property ) ) {
                    if ( ic.search.topics[ property ].label.toLowerCase() === $( this ).val().toLowerCase() ) {
                        filter = ic.search.topics[ property ].data;
                    }
                }
            }
        } );

        $( "body" ).on( "change", "#select_phone_main", function() {
            if ( $( this ).get( 0 ).selectedIndex !== 0 ) {
                relatedQuestions( "phone" );
            } else {
                $( "#phone" ).closest( "details" ).find( ".ic-rltd-q" ).fadeOut();
            }
        } );

        $( "body" ).on( "change", "#sub_phone", function() {
            if ( $( "#sub_phone" ).val() === dic.ito ) {
                $( "#phone" ).closest( "details" ).find( ".ic-rltd-q" ).fadeOut();
                return;
            }
        } );

        $( "body" ).on( "change", "#subject_phone", function() {
            if ( $( this ).get( 0 ).selectedIndex !== 0 ) {
                queryAddition = $( this ).val();
            } else {
                queryAddition = "";
            }

            relatedQuestions( "phone" );
        } );

        $( "body" ).on( "change", ".select_main", function() {
            sTypeVal = $( this ).closest( "details" ).children( "summary" ).attr( "id" );
            divToAPpendTo = "#" + sTypeVal;

            deconstruct( 2 );
            buildSubDP( $( this ).val() );
        } );

        $( "body" ).on( "change", ".select_sub_main", function() {
            sTypeVal = $( this ).closest( "details" ).children( "summary" ).attr( "id" );
            divToAPpendTo = "#" + sTypeVal;

            deconstruct( 1 );
            buildSubjectDP( $( "#select_" + sTypeVal  + "_main" ).val() );
        } );

        $( "body" ).on( "change", ".select_subject", function() {
            sTypeVal = $( this ).closest( "details" ).children( "summary" ).attr( "id" );
            divToAPpendTo = "#" + sTypeVal;

            buildSubSubjectDP( $( "#select_" + sTypeVal  + "_main" ).val() );
        } );

        $( "body" ).on( "change", ".select_sub_subject", function() {
            sTypeVal = $( this ).closest( "details" ).children( "summary" ).attr( "id" );
            divToAPpendTo = "#" + sTypeVal;

            buildLastBinders( $( "#select_" + sTypeVal + "_main" ).val(), $( "#sub_" + sTypeVal ).val(), $( this ).prop( "selectedIndex" ) + 1, $( this ).val(), $( "#subject_" + sTypeVal ).val() );
        } );

        // Since the option don't have specific value, we have to look for the inner text. Values should be use here for maintenance purposes.
        if ( sFromVal !== false ) {
            $( "#select_" + sTypeVal + "_main option:contains('" + sFromVal + "')" ).prop( "selected", true );
            $( "#select_" + sTypeVal + "_main" ).change();
        }

        if ( sSituationVal !== false ) {
            $( "#sub_" + sTypeVal + " option:contains('" + sSituationVal + "')" ).prop( "selected", true );
            $( "#sub_" + sTypeVal ).change();
        }

        if ( sEnquiryVal !== false ) {
            $( "#subject_" + sTypeVal + " option:contains('" + sEnquiryVal + "')" ).prop( "selected", true );
            $( "#subject_" + sTypeVal ).change();
        }

        // Using the aria-controls attribute created by expand/collapse js for toggling section
        if ( $( "html" ).hasClass( "no-details" ) ) {
            $trgt = $( "#" + sTypeVal );

            if ( $trgt.hasClass( "wb-details-inited" ) ) {
                $( "#" + sTypeVal ).trigger( "click" );
            } else {
                $( document ).on( "wb-ready.wb", function() {
                    $( "#" + sTypeVal ).trigger( "click" );
                } );
            }
        }

        // Test
        $( window ).on( "hashchange", function() {
            var $target = $( "#" + urlParam( "type" ) );

            $( ".ic-details details[open] summary" ).not( $target ).trigger( "click" );

            if ( !$target.parent().is( "[open]" ) ) {
                $target.trigger( "click" );
            }
        } );

        // On expand
        $( "main summary" ).not( ".ic-no-construct" ).on( "click", function() {
            construct( $( this ).attr( "id" ) );
        } );

        // First time expanding, pre-select based on url values
        if ( sFromVal || sSituationVal || sEnquiryVal ) {
            $( "main summary" ).one( "click", function() {
                var from = sFromVal,
                    type = $( this ).attr( "id" ),
                    situation = sSituationVal,
                    enquiry = sEnquiryVal;

                // FIXME: Dupe
                if ( from !== false ) {
                    $( "#select_" + type + "_main option:contains('" + from + "')" ).prop( "selected", true );
                    $( "#select_" + type + "_main" ).change();
                }

                if ( situation !== false ) {
                    $( "#sub_" + type + " option:contains('" + situation + "')" ).prop( "selected", true );
                    $( "#sub_" + type ).change();
                }

                if ( enquiry !== false ) {
                    $( "#subject_" + type + " option:contains('" + enquiry + "')" ).prop( "selected", true );
                    $( "#subject_" + type ).change();
                }
            } );
        }

        // EPI-11752: International phone numbers
        $( document ).on( "change", "#net_international", function() {
            var $this = $( this );

            if ( $this.prop( "checked" ) ) {
                /* Show the "international phone" section */
                $( ".international_Phone, .international_Fax" ).show();

                /* Hide the "national phone" section */
                $( ".national_Phone, .national_Business, .national_Fax" ).hide();

                /* Set the "national phone" fields to all zeroes */
                $( "#net_PhoneArea, #net_FaxArea" ).val( "000" );
                $( "#net_Phone, #net_BNumber, #net_Fax" ).val( "000-0000" );
            } else {
                /* Hide the "international phone" section */
                $( ".international_Phone, .international_Fax" ).hide();

                /* Clear the "national phone" fields */
                $( "#net_PhoneArea, #net_FaxArea" ).val( "" );
                $( "#net_Phone, #net_BNumber, #net_Fax" ).val( "" );

                /* Show the "national phone" section */
                $( ".national_Phone, .national_Business, .national_Fax" ).show();
            }
        } );

        // On selecting if you want a response or not to your feedback
        $( "body" ).on( "change", "#net_question_0", function() {
            $( "#contact-info-box" ).show();
        } );
        $( "body" ).on( "change", "#net_question_1", function() {
            $( "#contact-info-box" ).hide();
        } );

        $( "body" ).on( "click", "#attachment", function() {
            window.open( "/eic/registration.nsf/FULoadWind?openagent&amp;sid=" + sid + "&amp;fid=YDES-8VAL9G&amp;lang=" + lang.slice( 0, 1 ) + "&amp;aid=n039", "newWin", "menubar=no,scrollbars=yes,status=yes,resizable=yes,width=350,height=350" );
        } );
    }() );
} );
