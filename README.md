Repository for icgc.nsf-only features
===========================================

This is the code for features that are used only on icgc.nsf pages.
Current examples of such features are:

* Contact Us (h_07026.html)
* Google Search (06957.html)

Installation
-------------------------------------------

Required:

* Install NodeJS ( http://nodejs.org/ )
* Install Git ( http://git-scm.com/ )

Optionally, install:

* Git Credential Manager for Windows ( https://github.com/Microsoft/Git-Credential-Manager-for-Windows/releases )  
  This is especially useful if you're using brackets-git, since it provides a secure storage solution for your
  git credentials.

* Brackets ( http://brackets.io/ )
    * brackets-git ( https://github.com/zaggino/brackets-git )
    * brackets-grunt ( https://code.google.com/p/epic-eeic/source/list?repo=brackets-grunt )
    * brackets-jshint ( https://github.com/cfjedimaster/brackets-jshint )
    * brackets-jscs ( https://github.com/globexdesigns/brackets-jscs )
      Note: this extension needs to be built or installed from Bracket's Extension Manager

If behind a proxy ( see: https://github.com/adobe/brackets/wiki/How-to-Use-Brackets#preferences ):

* Allow Brackets' Extension Manager to connect to the repository
    * Debug > Open Preferences File
    * Add the following, with the correct proxy information  
      ```"proxy": "http://username:password@proxy.com:8080",```

Configuration
-------------------------------------------

1. Via "Brackets Git" (or regular "Git"), clone https://bitbucket.org/epic-eeic/ic.git

2. Via the "Git" console, run npm install

3. Via "Brackets", run "Project > Grunt default" (or "grunt" via the "Git" console)

Making Changes
-------------------------------------------

The source files are located in the "src" folder and subfolders. Make your changes there.

To build your changes, run "Project > Grunt default" in Brackets (or "grunt" via the "Git" console). Supporting files will be placed in "dist"

At the time of writing, the generated files must be imported into the "Attachments" section of the icgc.nsf database

Notes
------------------------

Using "Git Credential Manager for Windows" ( https://github.com/Microsoft/Git-Credential-Manager-for-Windows/releases ) is the recommended approach
to store your git credentials if you don't want to type them in at every pull/push. It works with both "brackets-git" and the Git console.

If you're using brackets-git, you have the option of saving your credentials there, but they are stored in plain text and should be avoided.